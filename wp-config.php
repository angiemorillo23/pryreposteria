<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pryreposteria' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#XDhRaQ$-f;x+fRT^Oi$st3@y@MTM(6~z9@sJo! 7lq;%S8)0<|z[wFYlFRyuoZu' );
define( 'SECURE_AUTH_KEY',  'Kfx25>X0*&PiJ6_fIKWMmGtH4KU2nb9I!T~Pq_^>&i#G2TUH<=&0& h^>?]LY;+b' );
define( 'LOGGED_IN_KEY',    'mUF,w!1;>xEDBEHV&~^Ok[Cc/8Y3T3k`[eZo$:F0c-*ZMwOw*)u=3soNB<uD@ P[' );
define( 'NONCE_KEY',        'z6=3:&4zglf7]SBR8;[c--/W_*|9VV%*KacAb4u`f%61[N,AY<Wd^aD9?.-$GHiT' );
define( 'AUTH_SALT',        '[%_&Dsb^AGe1PSXXq%q>AWw&Qu11xoUMjyQ^^(v:4EjC^It2nl0wSnBO-Ucq2^=4' );
define( 'SECURE_AUTH_SALT', 'qGc1S)Tf<:Nf_SU+n-URdDAzP:79hAJxT<S]S.OzJMPB;3zX5.$xO1d=n6Gcie/9' );
define( 'LOGGED_IN_SALT',   'y.mxisRsg>fas{q&[W*td-ysu+b69p*cPB0pl>}NteDhe~9>n0NHf4rx&N.qzP0x' );
define( 'NONCE_SALT',       '>1BtU~CbUTsE![fc!Bu<.O@*!{ `.j^}H;;J!zvlS6?YZm+mKtcMpp`!UO#-<2,4' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
