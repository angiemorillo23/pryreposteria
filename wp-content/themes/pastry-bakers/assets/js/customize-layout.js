/*
** Scripts within the customizer controls window.
*/

(function( $ ) {
	wp.customize.bind( 'ready', function() {

	/*
	** Reusable Functions
	*/
		var optPrefix = '#customize-control-pastry_bakers_options-';
		
		// Label
		function pastry_bakers_customizer_label( id, title ) {

			// Site Identity

			if ( id === 'custom_logo' || id === 'site_icon' ) {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}

			// General Setting

			if ( id === 'pastry_bakers_scroll_hide' || id === 'pastry_bakers_preloader_hide' || id === 'pastry_bakers_products_per_row' || id === 'pastry_bakers_scroll_top_position' || id === 'pastry_bakers_woo_product_border_radius')  {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}

			// Trending Product

			if ( id === 'pastry_bakers_product_main_heading' )  {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}

			// Colors

			if ( id === 'pastry_bakers_theme_color' || id === 'background_color' || id === 'background_image' ) {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}

			// Header Image

			if ( id === 'header_image' ) {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}

			//  Header

			if ( id === 'pastry_bakers_topbar_text' || id === 'pastry_bakers_topbar_phone_text' || id === 'pastry_bakers_topbar_email_text' || id === 'pastry_bakers_header_button' ) {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}

			// Slider

			if ( id === 'pastry_bakers_top_slider_page1' ) {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}

			// Footer

			if ( id === 'pastry_bakers_footer_bg_image' || id === 'pastry_bakers_show_hide_copyright') {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}

			// Post Setting

			if ( id === 'pastry_bakers_post_page_title' ) {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}

			// Single Post Setting

			if ( id === 'pastry_bakers_single_post_navigation_show_hide' ) {
				$( '#customize-control-'+ id ).before('<li class="tab-title customize-control">'+ title  +'</li>');
			} else {
				$( '#customize-control-pastry_bakers_options-'+ id ).before('<li class="tab-title customize-control">'+ title +'</li>');
			}
			
		}


	/*
	** Tabs
	*/

	    // Site Identity
		pastry_bakers_customizer_label( 'custom_logo', 'Logo Setup' );
		pastry_bakers_customizer_label( 'site_icon', 'Favicon' );

		// General Setting
		pastry_bakers_customizer_label( 'pastry_bakers_preloader_hide', 'Preloader' );
		pastry_bakers_customizer_label( 'pastry_bakers_scroll_hide', 'Scroll To Top' );
		pastry_bakers_customizer_label( 'pastry_bakers_products_per_row', 'woocommerce Setting' );
		pastry_bakers_customizer_label( 'pastry_bakers_woo_product_border_radius', 'Woocommerce Product Border Radius' );
		pastry_bakers_customizer_label( 'pastry_bakers_scroll_top_position', 'Scroll to top Position' );

		// Trending Product
		pastry_bakers_customizer_label( 'pastry_bakers_product_main_heading', 'Trending Product' );

		// Colors
		pastry_bakers_customizer_label( 'pastry_bakers_theme_color', 'Theme Color' );
		pastry_bakers_customizer_label( 'background_color', 'Colors' );
		pastry_bakers_customizer_label( 'background_image', 'Image' );

		//Header Image
		pastry_bakers_customizer_label( 'header_image', 'Header Image' );

		// Header
		pastry_bakers_customizer_label( 'pastry_bakers_topbar_text', 'Topbar Text' );
		pastry_bakers_customizer_label( 'pastry_bakers_topbar_phone_text', 'Phone Number' );
		pastry_bakers_customizer_label( 'pastry_bakers_topbar_email_text', 'Email' );
		pastry_bakers_customizer_label( 'pastry_bakers_header_button', 'Button' );

		//Slider
		pastry_bakers_customizer_label( 'pastry_bakers_top_slider_page1', 'Slider' );

		//Footer
		pastry_bakers_customizer_label( 'pastry_bakers_footer_bg_image', 'Footer' );
		pastry_bakers_customizer_label( 'pastry_bakers_show_hide_copyright', 'Copyright' );

		// Post Setting
		pastry_bakers_customizer_label( 'pastry_bakers_post_page_title', 'Post Setting' );

		//Single Post Setting
		pastry_bakers_customizer_label( 'pastry_bakers_single_post_navigation_show_hide', 'Single Post Setting' );
	

	}); // wp.customize ready

})( jQuery );
