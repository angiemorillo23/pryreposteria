function pastry_bakers_openNav() {
  jQuery(".sidenav").addClass('show');
}
function pastry_bakers_closeNav() {
  jQuery(".sidenav").removeClass('show');
}

( function( window, document ) {
  function pastry_bakers_keepFocusInMenu() {
    document.addEventListener( 'keydown', function( e ) {
      const pastry_bakers_nav = document.querySelector( '.sidenav' );

      if ( ! pastry_bakers_nav || ! pastry_bakers_nav.classList.contains( 'show' ) ) {
        return;
      }
      const elements = [...pastry_bakers_nav.querySelectorAll( 'input, a, button' )],
        pastry_bakers_lastEl = elements[ elements.length - 1 ],
        pastry_bakers_firstEl = elements[0],
        pastry_bakers_activeEl = document.activeElement,
        tabKey = e.keyCode === 9,
        shiftKey = e.shiftKey;

      if ( ! shiftKey && tabKey && pastry_bakers_lastEl === pastry_bakers_activeEl ) {
        e.preventDefault();
        pastry_bakers_firstEl.focus();
      }

      if ( shiftKey && tabKey && pastry_bakers_firstEl === pastry_bakers_activeEl ) {
        e.preventDefault();
        pastry_bakers_lastEl.focus();
      }
    } );
  }
  pastry_bakers_keepFocusInMenu();
} )( window, document );

var pastry_bakers_btn = jQuery('#button');

jQuery(window).scroll(function() {
  if (jQuery(window).scrollTop() > 300) {
    pastry_bakers_btn.addClass('show');
  } else {
    pastry_bakers_btn.removeClass('show');
  }
});

pastry_bakers_btn.on('click', function(e) {
  e.preventDefault();
  jQuery('html, body').animate({scrollTop:0}, '300');
});

jQuery(document).ready(function() {
  var owl = jQuery('#top-slider .owl-carousel');
    owl.owlCarousel({
    margin: 0,
    nav:true,
    autoplay : true,
    lazyLoad: true,
    autoplayTimeout: 5000,
    loop: false,
    dots: false,
    navText : ['<i class="fa fa-lg fa-caret-left"></i>','<i class="fa fa-lg fa-caret-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      576: {
        items: 1
      },
      768: {
        items: 1
      },
      1000: {
        items: 1
      },
      1200: {
        items: 1
      }
    },
    autoplayHoverPause : false,
    mouseDrag: true
  });

  var owl = jQuery('.product-sec .owl-carousel');
    owl.owlCarousel({
    margin: 20,
    nav:false,
    autoplay : true,
    lazyLoad: true,
    autoplayTimeout: 5000,
    loop: false,
    dots: false,
    navText : ['<i class="fa fa-lg fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-lg fa-chevron-right" aria-hidden="true"></i>'],
    responsive: {
      0: {
        items: 1
      },
      576: {
        items: 2
      },
      768: {
        items: 3
      },
      1000: {
        items: 4
      },
      1200: {
        items: 4
      }
    },
    autoplayHoverPause : false,
    mouseDrag: true
  });
})

window.addEventListener('load', (event) => {
  jQuery(".loading").delay(2000).fadeOut("slow");
});