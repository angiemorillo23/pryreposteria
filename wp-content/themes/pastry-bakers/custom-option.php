<?php

    $pastry_bakers_theme_css= "";

    /*--------------------------- Scroll to top positions -------------------*/

    $pastry_bakers_scroll_position = get_theme_mod( 'pastry_bakers_scroll_top_position','Right');
    if($pastry_bakers_scroll_position == 'Right'){
        $pastry_bakers_theme_css .='#button{';
            $pastry_bakers_theme_css .='right: 20px;';
        $pastry_bakers_theme_css .='}';
    }else if($pastry_bakers_scroll_position == 'Left'){
        $pastry_bakers_theme_css .='#button{';
            $pastry_bakers_theme_css .='left: 20px;';
        $pastry_bakers_theme_css .='}';
    }else if($pastry_bakers_scroll_position == 'Center'){
        $pastry_bakers_theme_css .='#button{';
            $pastry_bakers_theme_css .='right: 50%;left: 50%;';
        $pastry_bakers_theme_css .='}';
    }

    /*---------------------------Slider Height ------------*/

    $pastry_bakers_slider_img_height = get_theme_mod('pastry_bakers_slider_img_height');
    if($pastry_bakers_slider_img_height != false){
        $pastry_bakers_theme_css .='#top-slider .owl-carousel .owl-item img{';
            $pastry_bakers_theme_css .='height: '.esc_attr($pastry_bakers_slider_img_height).';';
        $pastry_bakers_theme_css .='}';
    }

    /*---------------- Single post Settings ------------------*/

    $pastry_bakers_single_post_navigation_show_hide = get_theme_mod('pastry_bakers_single_post_navigation_show_hide',true);
    if($pastry_bakers_single_post_navigation_show_hide != true){
        $pastry_bakers_theme_css .='.nav-links{';
            $pastry_bakers_theme_css .='display: none;';
        $pastry_bakers_theme_css .='}';
    }

    /*--------------------------- Woocommerce Product Border Radius -------------------*/

    $pastry_bakers_woo_product_border_radius = get_theme_mod('pastry_bakers_woo_product_border_radius', 0);
    if($pastry_bakers_woo_product_border_radius != false){
        $pastry_bakers_theme_css .='.woocommerce ul.products li.product a img{';
            $pastry_bakers_theme_css .='border-radius: '.esc_attr($pastry_bakers_woo_product_border_radius).'px;';
        $pastry_bakers_theme_css .='}';
    }

    /*--------------------------- Footer background image -------------------*/

    $pastry_bakers_footer_bg_image = get_theme_mod('pastry_bakers_footer_bg_image');
    if($pastry_bakers_footer_bg_image != false){
        $pastry_bakers_theme_css .='#colophon{';
            $pastry_bakers_theme_css .='background: url('.esc_attr($pastry_bakers_footer_bg_image).')!important;';
        $pastry_bakers_theme_css .='}';
    }

    /*--------------------------- Copyright Background Color -------------------*/

    $pastry_bakers_copyright_background_color = get_theme_mod('pastry_bakers_copyright_background_color');
    if($pastry_bakers_copyright_background_color != false){
        $pastry_bakers_theme_css .='.footer_info{';
            $pastry_bakers_theme_css .='background-color: '.esc_attr($pastry_bakers_copyright_background_color).' !important;';
        $pastry_bakers_theme_css .='}';
    } 

    /*--------------------------- Site Title And Tagline Color -------------------*/

    $pastry_bakers_logo_title_color = get_theme_mod('pastry_bakers_logo_title_color');
    if($pastry_bakers_logo_title_color != false){
        $pastry_bakers_theme_css .='p.site-title a, .navbar-brand a{';
            $pastry_bakers_theme_css .='color: '.esc_attr($pastry_bakers_logo_title_color).' !important;';
        $pastry_bakers_theme_css .='}';
    }

    $pastry_bakers_logo_tagline_color = get_theme_mod('pastry_bakers_logo_tagline_color');
    if($pastry_bakers_logo_tagline_color != false){
        $pastry_bakers_theme_css .='.logo p.site-description, .navbar-brand p{';
            $pastry_bakers_theme_css .='color: '.esc_attr($pastry_bakers_logo_tagline_color).'  !important;';
        $pastry_bakers_theme_css .='}';
    }