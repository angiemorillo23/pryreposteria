<?php
/**
 * Pastry Bakers functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Pastry Bakers
 */

include get_theme_file_path( 'vendor/wptrt/autoload/src/Pastry_Bakers_Loader.php' );

$Pastry_Bakers_Loader = new \WPTRT\Autoload\Pastry_Bakers_Loader();

$Pastry_Bakers_Loader->pastry_bakers_add( 'WPTRT\\Customize\\Section', get_theme_file_path( 'vendor/wptrt/customize-section-button/src' ) );

$Pastry_Bakers_Loader->pastry_bakers_register();

if ( ! function_exists( 'pastry_bakers_setup' ) ) :

	function pastry_bakers_setup() {

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		*/
		add_theme_support( 'post-formats', array('image','video','gallery','audio',) );

		load_theme_textdomain( 'pastry-bakers', get_template_directory() . '/languages' );
		add_theme_support( 'woocommerce' );
		add_theme_support( "responsive-embeds" );
		add_theme_support( "align-wide" );
		add_theme_support( "wp-block-styles" );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
        add_image_size('pastry-bakers-featured-header-image', 2000, 660, true);

        register_nav_menus( array(
            'primary' => esc_html__( 'Primary','pastry-bakers' ),
	        'footer'=> esc_html__( 'Footer Menu','pastry-bakers' ),
        ) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-background', apply_filters( 'pastry_bakers_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'custom-logo', array(
			'height'      => 50,
			'width'       => 50,
			'flex-width'  => true,
		) );

		add_editor_style( array( '/editor-style.css' ) );
		add_action( 'wp_ajax_pastry_bakers_dismissable_notice', 'pastry_bakers_dismissable_notice' );
	}
endif;
add_action( 'after_setup_theme', 'pastry_bakers_setup' );


function pastry_bakers_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'pastry_bakers_content_width', 1170 );
}
add_action( 'after_setup_theme', 'pastry_bakers_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pastry_bakers_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'pastry-bakers' ),
		'id'            => 'sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'pastry-bakers' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title">',
		'after_title'   => '</h5>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 1', 'pastry-bakers' ),
		'id'            => 'pastry-bakers-footer1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h5 class="footer-column-widget-title">',
		'after_title'   => '</h5>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 2', 'pastry-bakers' ),
		'id'            => 'pastry-bakers-footer2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h5 class="footer-column-widget-title">',
		'after_title'   => '</h5>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 3', 'pastry-bakers' ),
		'id'            => 'pastry-bakers-footer3',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h5 class="footer-column-widget-title">',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'pastry_bakers_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function pastry_bakers_scripts() {

	require_once get_theme_file_path( 'inc/wptt-webfont-loader.php' );

	wp_enqueue_style(
		'inter',
		wptt_get_webfont_url( 'https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap' ),
		array(),
		'1.0'
	);

	wp_enqueue_style(
		'garamond',
		wptt_get_webfont_url( 'https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@0,400;0,500;0,600;0,700;0,800;1,400;1,500;1,600;1,700;1,800&display=swap' ),
		array(),
		'1.0'
	);

	wp_enqueue_style( 'pastry-bakers-block-editor-style', get_theme_file_uri('/assets/css/block-editor-style.css') );

	// load bootstrap css
    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.css');

    wp_enqueue_style( 'owl.carousel-css', get_template_directory_uri() . '/assets/css/owl.carousel.css');

	wp_enqueue_style( 'pastry-bakers-style', get_stylesheet_uri() );
	require get_parent_theme_file_path( '/custom-option.php' );
	wp_add_inline_style( 'pastry-bakers-style',$pastry_bakers_theme_css );

	// fontawesome
	wp_enqueue_style( 'fontawesome-style', get_template_directory_uri() .'/assets/css/fontawesome/css/all.css' );

    wp_enqueue_script('pastry-bakers-theme-js', get_template_directory_uri() . '/assets/js/theme-script.js', array('jquery'), '', true );

    wp_enqueue_script('owl.carousel-js', get_template_directory_uri() . '/assets/js/owl.carousel.js', array('jquery'), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'pastry_bakers_scripts' );

/**
 * Enqueue Preloader.
 */
function pastry_bakers_preloader() {

  $pastry_bakers_theme_color_css = '';
  $pastry_bakers_preloader_bg_color = get_theme_mod('pastry_bakers_preloader_bg_color');
  $pastry_bakers_preloader_dot_1_color = get_theme_mod('pastry_bakers_preloader_dot_1_color');
  $pastry_bakers_preloader_dot_2_color = get_theme_mod('pastry_bakers_preloader_dot_2_color');
  $pastry_bakers_logo_max_height = get_theme_mod('pastry_bakers_logo_max_height');

  	if(get_theme_mod('pastry_bakers_logo_max_height') == '') {
		$pastry_bakers_logo_max_height = '24';
	}

	if(get_theme_mod('pastry_bakers_preloader_bg_color') == '') {
		$pastry_bakers_preloader_bg_color = '#d9a528';
	}
	if(get_theme_mod('pastry_bakers_preloader_dot_1_color') == '') {
		$pastry_bakers_preloader_dot_1_color = '#ffffff';
	}
	if(get_theme_mod('pastry_bakers_preloader_dot_2_color') == '') {
		$pastry_bakers_preloader_dot_2_color = '#111111';
	}
	$pastry_bakers_theme_color_css = '
		.custom-logo-link img{
			max-height: '.esc_attr($pastry_bakers_logo_max_height).'px;
	 	}
		.loading{
			background-color: '.esc_attr($pastry_bakers_preloader_bg_color).';
		 }
		 @keyframes loading {
		  0%,
		  100% {
		  	transform: translatey(-2.5rem);
		    background-color: '.esc_attr($pastry_bakers_preloader_dot_1_color).';
		  }
		  50% {
		  	transform: translatey(2.5rem);
		    background-color: '.esc_attr($pastry_bakers_preloader_dot_2_color).';
		  }
		}
	';
    wp_add_inline_style( 'pastry-bakers-style',$pastry_bakers_theme_color_css );

}
add_action( 'wp_enqueue_scripts', 'pastry_bakers_preloader' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/* TGM. */
require get_parent_theme_file_path( '/inc/tgm.php' );

function pastry_bakers_sanitize_select( $input, $setting ){
    $input = sanitize_key($input);
    $choices = $setting->manager->get_control( $setting->id )->choices;
    return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

function pastry_bakers_sanitize_checkbox( $input ) {
  // Boolean check
  return ( ( isset( $input ) && true == $input ) ? true : false );
}

/*radio button sanitization*/
function pastry_bakers_sanitize_choices( $input, $setting ) {
    global $wp_customize;
    $control = $wp_customize->get_control( $setting->id );
    if ( array_key_exists( $input, $control->choices ) ) {
        return $input;
    } else {
        return $setting->default;
    }
}

function pastry_bakers_sanitize_number_absint( $number, $setting ) {
	// Ensure $number is an absolute integer (whole number, zero or greater).
	$number = absint( $number );

	// If the input is an absolute integer, return it; otherwise, return the default
	return ( $number ? $number : $setting->default );
}

function pastry_bakers_sanitize_number_range( $number, $setting ) {
	
	// Ensure input is an absolute integer.
	$number = absint( $number );
	
	// Get the input attributes associated with the setting.
	$atts = $setting->manager->get_control( $setting->id )->input_attrs;
	
	// Get minimum number in the range.
	$min = ( isset( $atts['min'] ) ? $atts['min'] : $number );
	
	// Get maximum number in the range.
	$max = ( isset( $atts['max'] ) ? $atts['max'] : $number );
	
	// Get step.
	$step = ( isset( $atts['step'] ) ? $atts['step'] : 1 );
	
	// If the number is within the valid range, return it; otherwise, return the default
	return ( $min <= $number && $number <= $max && is_int( $number / $step ) ? $number : $setting->default );
}

/*dropdown page sanitization*/
function pastry_bakers_sanitize_dropdown_pages( $page_id, $setting ) {
	$page_id = absint( $page_id );
	return ( 'publish' == get_post_status( $page_id ) ? $page_id : $setting->default );
}

// Change number or products per row to 3
add_filter('loop_shop_columns', 'pastry_bakers_loop_columns');
if (!function_exists('pastry_bakers_loop_columns')) {
	function pastry_bakers_loop_columns() {
		$columns = get_theme_mod( 'pastry_bakers_products_per_row', 3 );
		return $columns; // 3 products per row
	}
}

if (!function_exists('pastry_bakers_loop_columns')) {
		function pastry_bakers_loop_columns() {
		return 3;
	}
}
add_filter('loop_shop_columns', 'pastry_bakers_loop_columns');

/**
 * Get CSS
 */

function pastry_bakers_getpage_css($hook) {
	wp_enqueue_script( 'pastry-bakers-admin-script', get_template_directory_uri() . '/inc/admin/js/pastry-bakers-admin-notice-script.js', array( 'jquery' ) );
    wp_localize_script( 'pastry-bakers-admin-script', 'pastry_bakers_ajax_object',
        array( 'ajax_url' => admin_url( 'admin-ajax.php' ) )
    );
	if ( 'appearance_page_pastry-bakers-info' != $hook ) {
		return;
	}
	wp_enqueue_style( 'pastry-bakers-demo-style', get_template_directory_uri() . '/assets/css/demo.css' );
}
add_action( 'admin_enqueue_scripts', 'pastry_bakers_getpage_css' );

if ( ! defined( 'PASTRY_BAKERS_CONTACT_SUPPORT' ) ) {
define('PASTRY_BAKERS_CONTACT_SUPPORT',__('https://wordpress.org/support/theme/pastry-bakers','pastry-bakers'));
}
if ( ! defined( 'PASTRY_BAKERS_REVIEW' ) ) {
define('PASTRY_BAKERS_REVIEW',__('https://wordpress.org/support/theme/pastry-bakers/reviews/#new-post','pastry-bakers'));
}
if ( ! defined( 'PASTRY_BAKERS_LIVE_DEMO' ) ) {
define('PASTRY_BAKERS_LIVE_DEMO',__('https://www.themagnifico.net/demo/pastry-bakers/','pastry-bakers'));
}
if ( ! defined( 'PASTRY_BAKERS_GET_PREMIUM_PRO' ) ) {
define('PASTRY_BAKERS_GET_PREMIUM_PRO',__('https://www.themagnifico.net/themes/bakers-wordpress-theme/','pastry-bakers'));
}
if ( ! defined( 'PASTRY_BAKERS_PRO_DOC' ) ) {
define('PASTRY_BAKERS_PRO_DOC',__('https://www.themagnifico.net/eard/wathiqa/pastry-bakers-pro-doc/','pastry-bakers'));
}
if ( ! defined( 'PASTRY_BAKERS_FREE_DOC' ) ) {
define('PASTRY_BAKERS_FREE_DOC',__('https://www.themagnifico.net/eard/wathiqa/pastry-bakers-free-doc/','pastry-bakers'));
}

add_action('admin_menu', 'pastry_bakers_themepage');
function pastry_bakers_themepage(){
	$pastry_bakers_theme_test = wp_get_theme();

	$pastry_bakers_theme_info = add_theme_page( __('Theme Options','pastry-bakers'), __('Theme Options','pastry-bakers'), 'manage_options', 'pastry-bakers-info.php', 'pastry_bakers_info_page' );
}

function pastry_bakers_info_page() {
	$pastry_bakers_user = wp_get_current_user();
	$pastry_bakers_theme = wp_get_theme();
	?>
	<div class="wrap about-wrap pastry-bakers-add-css">
		<div>
			<h1>
				<?php esc_html_e('Welcome To ','pastry-bakers'); ?><?php echo esc_html( $pastry_bakers_theme ); ?>
			</h1>
			<div class="feature-section three-col">
				<div class="col">
					<div class="widgets-holder-wrap">
						<h3><?php esc_html_e("Contact Support", "pastry-bakers"); ?></h3>
						<p><?php esc_html_e("Thank you for trying Online Tutor , feel free to contact us for any support regarding our theme.", "pastry-bakers"); ?></p>
						<p><a target="_blank" href="<?php echo esc_url( PASTRY_BAKERS_CONTACT_SUPPORT ); ?>" class="button button-primary get">
							<?php esc_html_e("Contact Support", "pastry-bakers"); ?>
						</a></p>
					</div>
				</div>
				<div class="col">
					<div class="widgets-holder-wrap">
						<h3><?php esc_html_e("Checkout Premium", "pastry-bakers"); ?></h3>
						<p><?php esc_html_e("Our premium theme comes with extended features like demo content import , responsive layouts etc.", "pastry-bakers"); ?></p>
						<p><a target="_blank" href="<?php echo esc_url( PASTRY_BAKERS_GET_PREMIUM_PRO ); ?>" class="button button-primary get">
							<?php esc_html_e("Get Premium", "pastry-bakers"); ?>
						</a></p>
					</div>
				</div>
				<div class="col">
					<div class="widgets-holder-wrap">
						<h3><?php esc_html_e("Review", "pastry-bakers"); ?></h3>
						<p><?php esc_html_e("If You love Online Tutor theme then we would appreciate your review about our theme.", "pastry-bakers"); ?></p>
						<p><a target="_blank" href="<?php echo esc_url( PASTRY_BAKERS_REVIEW ); ?>" class="button button-primary get">
							<?php esc_html_e("Review", "pastry-bakers"); ?>
						</a></p>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<h2><?php esc_html_e("Free Vs Premium","pastry-bakers"); ?></h2>
		<div class="pastry-bakers-button-container">
			<a target="_blank" href="<?php echo esc_url( PASTRY_BAKERS_PRO_DOC ); ?>" class="button button-primary get">
				<?php esc_html_e("Checkout Documentation", "pastry-bakers"); ?>
			</a>
			<a target="_blank" href="<?php echo esc_url( PASTRY_BAKERS_LIVE_DEMO ); ?>" class="button button-primary get">
				<?php esc_html_e("View Theme Demo", "pastry-bakers"); ?>
			</a>
		</div>
		<table class="wp-list-table widefat">
			<thead class="table-book">
				<tr>
					<th><strong><?php esc_html_e("Theme Feature", "pastry-bakers"); ?></strong></th>
					<th><strong><?php esc_html_e("Basic Version", "pastry-bakers"); ?></strong></th>
					<th><strong><?php esc_html_e("Premium Version", "pastry-bakers"); ?></strong></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php esc_html_e("Header Background Color", "pastry-bakers"); ?></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Custom Navigation Logo Or Text", "pastry-bakers"); ?></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Hide Logo Text", "pastry-bakers"); ?></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Premium Support", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Fully SEO Optimized", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Recent Posts Widget", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Easy Google Fonts", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Pagespeed Plugin", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Only Show Header Image On Front Page", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Show Header Everywhere", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Custom Text On Header Image", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Full Width (Hide Sidebar)", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Only Show Upper Widgets On Front Page", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Replace Copyright Text", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Customize Upper Widgets Colors", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Customize Navigation Color", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Customize Post/Page Color", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Customize Blog Feed Color", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Customize Footer Color", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Customize Sidebar Color", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Customize Background Color", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
				<tr>
					<td><?php esc_html_e("Importable Demo Content	", "pastry-bakers"); ?></td>
					<td><span class="cross"><span class="dashicons dashicons-dismiss"></span></span></td>
					<td><span class="tick"><span class="dashicons dashicons-yes-alt"></span></span></td>
				</tr>
			</tbody>
		</table>
		<div class="pastry-bakers-button-container">
			<a target="_blank" href="<?php echo esc_url( PASTRY_BAKERS_GET_PREMIUM_PRO ); ?>" class="button button-primary get">
				<?php esc_html_e("Go Premium", "pastry-bakers"); ?>
			</a>
		</div>
	</div>
	<?php
}

//Admin Notice For Getstart
function pastry_bakers_ajax_notice_handler() {
    if ( isset( $_POST['type'] ) ) {
        $type = sanitize_text_field( wp_unslash( $_POST['type'] ) );
        update_option( 'dismissed-' . $type, TRUE );
    }
}

function pastry_bakers_deprecated_hook_admin_notice() {

    $dismissed = get_user_meta(get_current_user_id(), 'pastry_bakers_dismissable_notice', true);
    if ( !$dismissed) { ?>
        <div class="updated notice notice-success is-dismissible notice-get-started-class" data-notice="get_started" style="background: #f7f9f9; padding: 20px 10px; display: flex;">
	    	<div class="tm-admin-image">
	    		<img style="width: 100%;max-width: 320px;line-height: 40px;display: inline-block;vertical-align: top;border: 2px solid #ddd;border-radius: 4px;" src="<?php echo esc_url(get_stylesheet_directory_uri()) .'/screenshot.png'; ?>" />
	    	</div>
	    	<div class="tm-admin-content" style="padding-left: 30px; align-self: center">
	    		<h2 style="font-weight: 600;line-height: 1.3; margin: 0px;"><?php esc_html_e('Thank You For Choosing ', 'pastry-bakers'); ?><?php echo wp_get_theme(); ?><h2>
	    		<p style="color: #3c434a; font-weight: 400; margin-bottom: 30px;"><?php _e('Get Started With Theme By Clicking On Getting Started.', 'pastry-bakers'); ?><p>
	        	<a class="admin-notice-btn button button-primary button-hero" href="<?php echo esc_url( admin_url( 'themes.php?page=pastry-bakers-info.php' )); ?>"><?php esc_html_e( 'Get started', 'pastry-bakers' ) ?></a>
	        	<a class="admin-notice-btn button button-primary button-hero" target="_blank" href="<?php echo esc_url( PASTRY_BAKERS_FREE_DOC ); ?>"><?php esc_html_e( 'Documentation', 'pastry-bakers' ) ?></a>
	        	<span style="padding-top: 15px; display: inline-block; padding-left: 8px;">
	        	<span class="dashicons dashicons-admin-links"></span>
	        	<a class="admin-notice-btn"	 target="_blank" href="<?php echo esc_url( PASTRY_BAKERS_LIVE_DEMO ); ?>"><?php esc_html_e( 'View Demo', 'pastry-bakers' ) ?></a>
	        	</span>
	    	</div>
        </div>
    <?php }
}

add_action( 'admin_notices', 'pastry_bakers_deprecated_hook_admin_notice' );

function pastry_bakers_switch_theme() {
    delete_user_meta(get_current_user_id(), 'pastry_bakers_dismissable_notice');
}
add_action('after_switch_theme', 'pastry_bakers_switch_theme');
function pastry_bakers_dismissable_notice() {
    update_user_meta(get_current_user_id(), 'pastry_bakers_dismissable_notice', true);
    die();
}