<?php
/**
 * Pastry Bakers Theme Customizer
 *
 * @link: https://developer.wordpress.org/themes/customize-api/customizer-objects/
 *
 * @package Pastry Bakers
 */

if ( ! defined( 'PASTRY_BAKERS_URL' ) ) {
    define( 'PASTRY_BAKERS_URL', esc_url( 'https://www.themagnifico.net/themes/bakers-wordpress-theme/', 'pastry-bakers') );
}
if ( ! defined( 'PASTRY_BAKERS_TEXT' ) ) {
    define( 'PASTRY_BAKERS_TEXT', __( 'Pastry Bakers Pro','pastry-bakers' ));
}
if ( ! defined( 'PASTRY_BAKERS_BUY_TEXT' ) ) {
    define( 'PASTRY_BAKERS_BUY_TEXT', __( 'Buy Pastry Bakers Pro','pastry-bakers' ));
}

use WPTRT\Customize\Section\Pastry_Bakers_Button;

add_action( 'customize_register', function( $manager ) {

    $manager->register_section_type( Pastry_Bakers_Button::class );

    $manager->add_section(
        new Pastry_Bakers_Button( $manager, 'pastry_bakers_pro', [
            'title'       => esc_html( PASTRY_BAKERS_TEXT,'pastry-bakers' ),
            'priority'    => 0,
            'button_text' => __( 'GET PREMIUM', 'pastry-bakers' ),
            'button_url'  => esc_url( PASTRY_BAKERS_URL )
        ] )
    );

} );

// Load the JS and CSS.
add_action( 'customize_controls_enqueue_scripts', function() {

    $version = wp_get_theme()->get( 'Version' );

    wp_enqueue_script(
        'pastry-bakers-customize-section-button',
        get_theme_file_uri( 'vendor/wptrt/customize-section-button/public/js/customize-controls.js' ),
        [ 'customize-controls' ],
        $version,
        true
    );

    wp_enqueue_style(
        'pastry-bakers-customize-section-button',
        get_theme_file_uri( 'vendor/wptrt/customize-section-button/public/css/customize-controls.css' ),
        [ 'customize-controls' ],
        $version
    );

} );

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function pastry_bakers_customize_register($wp_customize){

    // Pro Version
    class Pastry_Bakers_Customize_Pro_Version extends WP_Customize_Control {
        public $type = 'pro_options';

        public function render_content() {
            echo '<span>For More <strong>'. esc_html( $this->label ) .'</strong>?</span>';
            echo '<a href="'. esc_url($this->description) .'" target="_blank">';
                echo '<span class="dashicons dashicons-info"></span>';
                echo '<strong> '. esc_html( PASTRY_BAKERS_BUY_TEXT,'pastry-bakers' ) .'<strong></a>';
            echo '</a>';
        }
    }

    // Custom Controls
    function Pastry_Bakers_sanitize_custom_control( $input ) {
        return $input;
    }

    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport = 'postMessage';

     //Logo
    $wp_customize->add_setting('pastry_bakers_logo_max_height',array(
        'default'   => '24',
        'sanitize_callback' => 'pastry_bakers_sanitize_number_absint'
    ));
    $wp_customize->add_control('pastry_bakers_logo_max_height',array(
        'label' => esc_html__('Logo Width','pastry-bakers'),
        'section'   => 'title_tagline',
        'type'      => 'number'
    ));

    $wp_customize->add_setting('pastry_bakers_logo_title_text', array(
        'default' => true,
        'sanitize_callback' => 'pastry_bakers_sanitize_checkbox'
    ));
    $wp_customize->add_control( new WP_Customize_Control($wp_customize,'pastry_bakers_logo_title_text',array(
        'label'          => __( 'Enable Disable Title', 'pastry-bakers' ),
        'section'        => 'title_tagline',
        'settings'       => 'pastry_bakers_logo_title_text',
        'type'           => 'checkbox',
    )));

    $wp_customize->add_setting('pastry_bakers_theme_description', array(
        'default' => false,
        'sanitize_callback' => 'pastry_bakers_sanitize_checkbox'
    ));
    $wp_customize->add_control( new WP_Customize_Control($wp_customize,'pastry_bakers_theme_description',array(
        'label'          => __( 'Enable Disable Tagline', 'pastry-bakers' ),
        'section'        => 'title_tagline',
        'settings'       => 'pastry_bakers_theme_description',
        'type'           => 'checkbox',
    )));

    $wp_customize->add_setting('pastry_bakers_logo_title_color', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_hex_color',
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pastry_bakers_logo_title_color', array(
        'label'    => __('Site Title Color', 'pastry-bakers'),
        'section'  => 'title_tagline'
    )));

    $wp_customize->add_setting('pastry_bakers_logo_tagline_color', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_hex_color',
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pastry_bakers_logo_tagline_color', array(
        'label'    => __('Site Tagline Color', 'pastry-bakers'),
        'section'  => 'title_tagline'
    )));

    // Pro Version
    $wp_customize->add_setting( 'pro_version_logo', array(
        'sanitize_callback' => 'Pastry_Bakers_sanitize_custom_control'
    ));
    $wp_customize->add_control( new Pastry_Bakers_Customize_Pro_Version ( $wp_customize,'pro_version_logo', array(
        'section'     => 'title_tagline',
        'type'        => 'pro_options',
        'label'       => esc_html__( 'Customizer Options', 'pastry-bakers' ),
        'description' => esc_url( PASTRY_BAKERS_URL ),
        'priority'    => 100
    )));

    // General Settings
     $wp_customize->add_section('pastry_bakers_general_settings',array(
        'title' => esc_html__('General Settings','pastry-bakers'),
        'priority'   => 30,
    ));

    $wp_customize->add_setting('pastry_bakers_preloader_hide', array(
        'default' => 0,
        'sanitize_callback' => 'pastry_bakers_sanitize_checkbox'
    ));
    $wp_customize->add_control( new WP_Customize_Control($wp_customize,'pastry_bakers_preloader_hide',array(
        'label'          => __( 'Show Theme Preloader', 'pastry-bakers' ),
        'section'        => 'pastry_bakers_general_settings',
        'settings'       => 'pastry_bakers_preloader_hide',
        'type'           => 'checkbox',
    )));

    $wp_customize->add_setting( 'pastry_bakers_preloader_bg_color', array(
        'default' => '#d9a528',
        'sanitize_callback' => 'sanitize_hex_color'
    ));
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'pastry_bakers_preloader_bg_color', array(
        'label' => esc_html__('Preloader Background Color','pastry-bakers'),
        'section' => 'pastry_bakers_general_settings',
        'settings' => 'pastry_bakers_preloader_bg_color'
    )));

    $wp_customize->add_setting( 'pastry_bakers_preloader_dot_1_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color'
    ));
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'pastry_bakers_preloader_dot_1_color', array(
        'label' => esc_html__('Preloader First Dot Color','pastry-bakers'),
        'section' => 'pastry_bakers_general_settings',
        'settings' => 'pastry_bakers_preloader_dot_1_color'
    )));

    $wp_customize->add_setting( 'pastry_bakers_preloader_dot_2_color', array(
        'default' => '#111111',
        'sanitize_callback' => 'sanitize_hex_color'
    ));
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'pastry_bakers_preloader_dot_2_color', array(
        'label' => esc_html__('Preloader Second Dot Color','pastry-bakers'),
        'section' => 'pastry_bakers_general_settings',
        'settings' => 'pastry_bakers_preloader_dot_2_color'
    )));

    $wp_customize->add_setting('pastry_bakers_scroll_hide', array(
        'default' => false,
        'sanitize_callback' => 'pastry_bakers_sanitize_checkbox'
    ));
    $wp_customize->add_control( new WP_Customize_Control($wp_customize,'pastry_bakers_scroll_hide',array(
        'label'          => __( 'Show Theme Scroll To Top', 'pastry-bakers' ),
        'section'        => 'pastry_bakers_general_settings',
        'settings'       => 'pastry_bakers_scroll_hide',
        'type'           => 'checkbox',
    )));

    $wp_customize->add_setting('pastry_bakers_scroll_top_position',array(
        'default' => 'Right',
        'sanitize_callback' => 'pastry_bakers_sanitize_choices'
    ));
    $wp_customize->add_control('pastry_bakers_scroll_top_position',array(
        'type' => 'radio',
        'section' => 'pastry_bakers_general_settings',
        'choices' => array(
            'Right' => __('Right','pastry-bakers'),
            'Left' => __('Left','pastry-bakers'),
            'Center' => __('Center','pastry-bakers')
        ),
    ) );

    // Product Columns
    $wp_customize->add_setting( 'pastry_bakers_products_per_row' , array(
       'default'           => '3',
       'transport'         => 'refresh',
       'sanitize_callback' => 'pastry_bakers_sanitize_select',
    ) );

    $wp_customize->add_control('pastry_bakers_products_per_row', array(
       'label' => __( 'Product per row', 'pastry-bakers' ),
       'section'  => 'pastry_bakers_general_settings',
       'type'     => 'select',
       'choices'  => array(
           '2' => '2',
           '3' => '3',
           '4' => '4',
       ),
    ) );

    //Products border radius
    $wp_customize->add_setting( 'pastry_bakers_woo_product_border_radius', array(
        'default'              => '0',
        'transport'            => 'refresh',
        'sanitize_callback'    => 'pastry_bakers_sanitize_number_range'
    ) );
    $wp_customize->add_control( 'pastry_bakers_woo_product_border_radius', array(
        'label'       => esc_html__( 'Product Border Radius','pastry-bakers' ),
        'section'     => 'pastry_bakers_general_settings',
        'type'        => 'range',
        'input_attrs' => array(
            'step'             => 1,
            'min'              => 1,
            'max'              => 150,
        ),
    ) );

    // Pro Version
    $wp_customize->add_setting( 'pro_version_general_setting', array(
        'sanitize_callback' => 'Pastry_Bakers_sanitize_custom_control'
    ));
    $wp_customize->add_control( new Pastry_Bakers_Customize_Pro_Version ( $wp_customize,'pro_version_general_setting', array(
        'section'     => 'pastry_bakers_general_settings',
        'type'        => 'pro_options',
        'label'       => esc_html__( 'Customizer Options', 'pastry-bakers' ),
        'description' => esc_url( PASTRY_BAKERS_URL ),
        'priority'    => 100
    )));

    //TopBar
    $wp_customize->add_section('pastry_bakers_topbar',array(
        'title' => esc_html__('Topbar Option','pastry-bakers')
    ));

    $wp_customize->add_setting('pastry_bakers_topbar_text',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_topbar_text',array(
        'label' => esc_html__('Topbar Text','pastry-bakers'),
        'section' => 'pastry_bakers_topbar',
        'setting' => 'pastry_bakers_topbar_text',
        'type'  => 'text'
    ));

    $wp_customize->add_setting('pastry_bakers_topbar_phone_text',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_topbar_phone_text',array(
        'label' => esc_html__('Phone Text','pastry-bakers'),
        'section' => 'pastry_bakers_topbar',
        'setting' => 'pastry_bakers_topbar_phone_text',
        'type'  => 'text'
    ));

    $wp_customize->add_setting('pastry_bakers_topbar_phone_number',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_topbar_phone_number',array(
        'label' => esc_html__('Phone Number','pastry-bakers'),
        'section' => 'pastry_bakers_topbar',
        'setting' => 'pastry_bakers_topbar_phone_number',
        'type'  => 'text'
    ));

    $wp_customize->add_setting('pastry_bakers_support_button_text',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_support_button_text',array(
        'label' => esc_html__('Support Button Text','pastry-bakers'),
        'section' => 'pastry_bakers_topbar',
        'setting' => 'pastry_bakers_support_button_text',
        'type'  => 'text'
    ));

    $wp_customize->add_setting('pastry_bakers_support_button_url',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_support_button_url',array(
        'label' => esc_html__('Support Button Url','pastry-bakers'),
        'section' => 'pastry_bakers_topbar',
        'setting' => 'pastry_bakers_support_button_url',
        'type'  => 'text'
    ));

    $wp_customize->add_setting('pastry_bakers_faq_button_text',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_faq_button_text',array(
        'label' => esc_html__('Faq Button Text','pastry-bakers'),
        'section' => 'pastry_bakers_topbar',
        'setting' => 'pastry_bakers_faq_button_text',
        'type'  => 'text'
    ));

    $wp_customize->add_setting('pastry_bakers_faq_button_url',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_faq_button_url',array(
        'label' => esc_html__('Faq Button Url','pastry-bakers'),
        'section' => 'pastry_bakers_topbar',
        'setting' => 'pastry_bakers_faq_button_url',
        'type'  => 'text'
    ));

    $wp_customize->add_setting('pastry_bakers_header_button_text',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_header_button_text',array(
        'label' => esc_html__('Header Button Text','pastry-bakers'),
        'section' => 'pastry_bakers_topbar',
        'setting' => 'pastry_bakers_header_button_text',
        'type'  => 'text'
    ));

    $wp_customize->add_setting('pastry_bakers_header_button_url',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_header_button_url',array(
        'label' => esc_html__('Header Button Url','pastry-bakers'),
        'section' => 'pastry_bakers_topbar',
        'setting' => 'pastry_bakers_header_button_url',
        'type'  => 'text'
    ));

    // Pro Version
    $wp_customize->add_setting( 'pro_version_topbar_setting', array(
        'sanitize_callback' => 'Pastry_Bakers_sanitize_custom_control'
    ));
    $wp_customize->add_control( new Pastry_Bakers_Customize_Pro_Version ( $wp_customize,'pro_version_topbar_setting', array(
        'section'     => 'pastry_bakers_topbar',
        'type'        => 'pro_options',
        'label'       => esc_html__( 'Customizer Options', 'pastry-bakers' ),
        'description' => esc_url( PASTRY_BAKERS_URL ),
        'priority'    => 100
    )));

     //Slider
    $wp_customize->add_section('pastry_bakers_top_slider',array(
        'title' => esc_html__('Slider Settings','pastry-bakers'),
        'description' => esc_html__('Here you have to add 3 different pages in below dropdown. Note: Image Dimensions 1400 x 550 px','pastry-bakers')
    ));

    $wp_customize->add_setting('pastry_bakers_slider_short_text',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_slider_short_text',array(
        'label' => esc_html__('Slider Text','pastry-bakers'),
        'section' => 'pastry_bakers_top_slider',
        'setting' => 'pastry_bakers_slider_short_text',
        'type'  => 'text'
    ));

    for ( $pastry_bakers_count = 1; $pastry_bakers_count <= 3; $pastry_bakers_count++ ) {

        $wp_customize->add_setting( 'pastry_bakers_top_slider_page' . $pastry_bakers_count, array(
            'default'           => '',
            'sanitize_callback' => 'pastry_bakers_sanitize_dropdown_pages'
        ) );
        $wp_customize->add_control( 'pastry_bakers_top_slider_page' . $pastry_bakers_count, array(
            'label'    => __( 'Select Slide Page', 'pastry-bakers' ),
            'section'  => 'pastry_bakers_top_slider',
            'type'     => 'dropdown-pages'
        ) );
    }

    //Slider height
    $wp_customize->add_setting('pastry_bakers_slider_img_height',array(
        'default'=> '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_slider_img_height',array(
        'label' => __('Slider Height','pastry-bakers'),
        'description'   => __('Add the slider height in px(eg. 500px).','pastry-bakers'),
        'input_attrs' => array(
            'placeholder' => __( '500px', 'pastry-bakers' ),
        ),
        'section'=> 'pastry_bakers_top_slider',
        'type'=> 'text'
    ));

    // Pro Version
    $wp_customize->add_setting( 'pro_version_top_slider_setting', array(
        'sanitize_callback' => 'Pastry_Bakers_sanitize_custom_control'
    ));
    $wp_customize->add_control( new Pastry_Bakers_Customize_Pro_Version ( $wp_customize,'pro_version_top_slider_setting', array(
        'section'     => 'pastry_bakers_top_slider',
        'type'        => 'pro_options',
        'label'       => esc_html__( 'Customizer Options', 'pastry-bakers' ),
        'description' => esc_url( PASTRY_BAKERS_URL ),
        'priority'    => 100
    )));

    // Trending Product
    $wp_customize->add_section('pastry_bakers_product_section',array(
        'title' => esc_html__('Trending Product Option','pastry-bakers'),
    ));

    $wp_customize->add_setting('pastry_bakers_product_main_heading',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_product_main_heading',array(
        'label' => esc_html__('Short Heading','pastry-bakers'),
        'section' => 'pastry_bakers_product_section',
        'setting' => 'pastry_bakers_product_main_heading',
        'type'  => 'text'
    ));

    $wp_customize->add_setting('pastry_bakers_product_main_discription',array(
        'default' => '',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('pastry_bakers_product_main_discription',array(
        'label' => esc_html__('Heading','pastry-bakers'),
        'section' => 'pastry_bakers_product_section',
        'setting' => 'pastry_bakers_product_main_discription',
        'type'  => 'text'
    ));
    
    if(class_exists('woocommerce')){

        $wp_customize->add_setting('pastry_bakers_home_product_per_page',array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field'
        ));
        $wp_customize->add_control('pastry_bakers_home_product_per_page',array(
            'label' => esc_html__('No Of Products','pastry-bakers'),
            'section' => 'pastry_bakers_product_section',
            'setting' => 'pastry_bakers_home_product_per_page',
            'type'  => 'number'
        ));
        
        $pastry_bakers_args = array(
           'type'                     => 'product',
            'child_of'                 => 0,
            'parent'                   => '',
            'orderby'                  => 'term_group',
            'order'                    => 'ASC',
            'hide_empty'               => false,
            'hierarchical'             => 1,
            'number'                   => '',
            'taxonomy'                 => 'product_cat',
            'pad_counts'               => false
        );
        $categories = get_categories( $pastry_bakers_args );
        $cats = array();
        $i = 0;
        foreach($categories as $category){
            if($i==0){
                $default = $category->slug;
                $i++;
            }
            $cats[$category->slug] = $category->name;
        }
        $wp_customize->add_setting('pastry_bakers_home_product',array(
            'sanitize_callback' => 'pastry_bakers_sanitize_select',
        ));
        $wp_customize->add_control('pastry_bakers_home_product',array(
            'type'    => 'select',
            'choices' => $cats,
            'label' => __('Select Product Category','pastry-bakers'),
            'section' => 'pastry_bakers_product_section',
        ));
    }

    // Pro Version
    $wp_customize->add_setting( 'pro_version_product_setting', array(
        'sanitize_callback' => 'Pastry_Bakers_sanitize_custom_control'
    ));
    $wp_customize->add_control( new Pastry_Bakers_Customize_Pro_Version ( $wp_customize,'pro_version_product_setting', array(
        'section'     => 'pastry_bakers_product_section',
        'type'        => 'pro_options',
        'label'       => esc_html__( 'Customizer Options', 'pastry-bakers' ),
        'description' => esc_url( PASTRY_BAKERS_URL ),
        'priority'    => 100
    )));

    // Post Settings
     $wp_customize->add_section('pastry_bakers_post_settings',array(
        'title' => esc_html__('Post Settings','pastry-bakers'),
        'priority'   =>40,
    ));

    $wp_customize->add_setting('pastry_bakers_post_page_title',array(
        'sanitize_callback' => 'pastry_bakers_sanitize_checkbox',
        'default'           => 1,
    ));
    $wp_customize->add_control('pastry_bakers_post_page_title',array(
        'type'        => 'checkbox',
        'label'       => esc_html__('Enable Post Page Title', 'pastry-bakers'),
        'section'     => 'pastry_bakers_post_settings',
        'description' => esc_html__('Check this box to enable title on post page.', 'pastry-bakers'),
    ));

    $wp_customize->add_setting('pastry_bakers_post_page_meta',array(
        'sanitize_callback' => 'pastry_bakers_sanitize_checkbox',
        'default'           => 1,
    ));
    $wp_customize->add_control('pastry_bakers_post_page_meta',array(
        'type'        => 'checkbox',
        'label'       => esc_html__('Enable Post Page Meta', 'pastry-bakers'),
        'section'     => 'pastry_bakers_post_settings',
        'description' => esc_html__('Check this box to enable meta on post page.', 'pastry-bakers'),
    ));

    $wp_customize->add_setting('pastry_bakers_post_page_thumb',array(
        'sanitize_callback' => 'pastry_bakers_sanitize_checkbox',
        'default'           => 1,
    ));
    $wp_customize->add_control('pastry_bakers_post_page_thumb',array(
        'type'        => 'checkbox',
        'label'       => esc_html__('Enable Post Page Thumbnail', 'pastry-bakers'),
        'section'     => 'pastry_bakers_post_settings',
        'description' => esc_html__('Check this box to enable thumbnail on post page.', 'pastry-bakers'),
    ));

    $wp_customize->add_setting('pastry_bakers_single_post_navigation_show_hide',array(
        'default' => true,
        'sanitize_callback' => 'pastry_bakers_sanitize_checkbox'
    ));
    $wp_customize->add_control('pastry_bakers_single_post_navigation_show_hide',array(
        'type' => 'checkbox',
        'label' => __('Show / Hide Post Navigation','pastry-bakers'),
        'section' => 'pastry_bakers_post_settings',
    ));

    // Pro Version
    $wp_customize->add_setting( 'pro_version_post_setting', array(
        'sanitize_callback' => 'Pastry_Bakers_sanitize_custom_control'
    ));
    $wp_customize->add_control( new Pastry_Bakers_Customize_Pro_Version ( $wp_customize,'pro_version_post_setting', array(
        'section'     => 'pastry_bakers_post_settings',
        'type'        => 'pro_options',
        'label'       => esc_html__( 'Customizer Options', 'pastry-bakers' ),
        'description' => esc_url( PASTRY_BAKERS_URL ),
        'priority'    => 100
    )));
    
    // Footer
    $wp_customize->add_section('pastry_bakers_site_footer_section', array(
        'title' => esc_html__('Footer', 'pastry-bakers'),
    ));

    $wp_customize->add_setting('pastry_bakers_footer_bg_image',array(
        'default'   => '',
        'sanitize_callback' => 'esc_url_raw',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize,'pastry_bakers_footer_bg_image',array(
        'label' => __('Footer Background Image','pastry-bakers'),
        'section' => 'pastry_bakers_site_footer_section',
        'priority' => 1,
    )));

    $wp_customize->add_setting('pastry_bakers_footer_text_setting', array(
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('pastry_bakers_footer_text_setting', array(
        'label' => __('Replace the footer text', 'pastry-bakers'),
        'section' => 'pastry_bakers_site_footer_section',
        'priority' => 1,
        'type' => 'text',
    ));

    $wp_customize->add_setting('pastry_bakers_show_hide_copyright',array(
        'default' => true,
        'sanitize_callback' => 'pastry_bakers_sanitize_checkbox'
    ));
    $wp_customize->add_control('pastry_bakers_show_hide_copyright',array(
        'type' => 'checkbox',
        'label' => __('Show / Hide Copyright','pastry-bakers'),
        'section' => 'pastry_bakers_site_footer_section',
    ));

    $wp_customize->add_setting('pastry_bakers_copyright_background_color', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_hex_color',
    ));
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pastry_bakers_copyright_background_color', array(
        'label'    => __('Copyright Background Color', 'pastry-bakers'),
        'section'  => 'pastry_bakers_site_footer_section',
    )));

    // Pro Version
    $wp_customize->add_setting( 'pro_version_footer_setting', array(
        'sanitize_callback' => 'Pastry_Bakers_sanitize_custom_control'
    ));
    $wp_customize->add_control( new Pastry_Bakers_Customize_Pro_Version ( $wp_customize,'pro_version_footer_setting', array(
        'section'     => 'pastry_bakers_site_footer_section',
        'type'        => 'pro_options',
        'label'       => esc_html__( 'Customizer Options', 'pastry-bakers' ),
        'description' => esc_url( PASTRY_BAKERS_URL ),
        'priority'    => 100
    )));

}
add_action('customize_register', 'pastry_bakers_customize_register');

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function pastry_bakers_customize_partial_blogname(){
    bloginfo('name');
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function pastry_bakers_customize_partial_blogdescription(){
    bloginfo('description');
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function pastry_bakers_customize_preview_js(){
    wp_enqueue_script('pastry-bakers-customizer', esc_url(get_template_directory_uri()) . '/assets/js/customizer.js', array('customize-preview'), '20151215', true);
}
add_action('customize_preview_init', 'pastry_bakers_customize_preview_js');

/*
** Load dynamic logic for the customizer controls area.
*/
function pastry_bakers_panels_js() {
    wp_enqueue_style( 'pastry-bakers-customizer-layout-css', get_theme_file_uri( '/assets/css/customizer-layout.css' ) );
    wp_enqueue_script( 'pastry-bakers-customize-layout', get_theme_file_uri( '/assets/js/customize-layout.js' ), array(), '1.2', true );
}
add_action( 'customize_controls_enqueue_scripts', 'pastry_bakers_panels_js' );