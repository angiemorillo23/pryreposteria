<?php

require get_template_directory() . '/inc/class-tgm-plugin-activation.php';

function pastry_bakers_recommended_plugins() {
	$plugins = array(
		array(
			'name'             => __( 'GTranslate', 'pastry-bakers' ),
			'slug'             => 'gtranslate',
			'source'           => '',
			'required'         => false,
			'force_activation' => false,
		),
	);
	$config = array();
	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'pastry_bakers_recommended_plugins' );