<?php
/**
 * Template Name: Home Template
 */

get_header(); ?>

<main id="skip-content">
  <section id="top-slider">
    <?php $pastry_bakers_slide_pages = array();
      for ( $pastry_bakers_count = 1; $pastry_bakers_count <= 3; $pastry_bakers_count++ ) {
        $pastry_bakers_mod = intval( get_theme_mod( 'pastry_bakers_top_slider_page' . $pastry_bakers_count ));
        if ( 'page-none-selected' != $pastry_bakers_mod ) {
          $pastry_bakers_slide_pages[] = $pastry_bakers_mod;
        }
      }
      if( !empty($pastry_bakers_slide_pages) ) :
        $pastry_bakers_args = array(
          'post_type' => 'page',
          'post__in' => $pastry_bakers_slide_pages,
          'orderby' => 'post__in'
        );
        $pastry_bakers_query = new WP_Query( $pastry_bakers_args );
        if ( $pastry_bakers_query->have_posts() ) :
          $i = 1;
    ?>
    <div class="owl-carousel" role="listbox">
      <?php  while ( $pastry_bakers_query->have_posts() ) : $pastry_bakers_query->the_post(); ?>
        <div class="slider-box">
          <?php if(has_post_thumbnail()){ ?><img src="<?php the_post_thumbnail_url('full'); ?>"/><?php }else { ?><div class="slide-bg"></div> <?php } ?>
          <div class="slider-inner-box">
            <?php if ( get_theme_mod('pastry_bakers_slider_short_text') != "" ) {?>
              <h3 class="m-0"><?php echo esc_html(get_theme_mod('pastry_bakers_slider_short_text')); ?></h3> 
            <?php }?>
            <h2 class="m-0"><?php the_title(); ?></h2>
            <p class="content my-4"><?php echo esc_html( wp_trim_words( get_the_content(),20 )); ?></p>
            <div class="slide-btn mt-4"><a href="<?php the_permalink(); ?>"><?php esc_html_e('Shop Now','pastry-bakers'); ?></a></div>
          </div>
        </div>
      <?php $i++; endwhile;
      wp_reset_postdata();?>
    </div>
    <?php else : ?>
      <div class="no-postfound"></div>
    <?php endif;
    endif;?>
  </section>
  <section class="product-sec py-5 px-5">
    <div class="container">
      <div class=" text-center headings mb-5">
        <?php if(get_theme_mod('pastry_bakers_product_main_heading') != ''){ ?>
          <p class="mb-2"><?php echo esc_html(get_theme_mod('pastry_bakers_product_main_heading','')); ?></p>
        <?php }?>
        <?php if(get_theme_mod('pastry_bakers_product_main_discription') != ''){ ?>
          <h3><?php echo esc_html(get_theme_mod('pastry_bakers_product_main_discription','')); ?></h3>
        <?php }?>
      </div>
      <div class="owl-carousel product-home-box">
        <?php
        if ( class_exists( 'WooCommerce' ) ) {
          $pastry_bakers_args = array(
            'post_type' => 'product',
            'posts_per_page' => get_theme_mod('pastry_bakers_home_product_per_page', 5),
            'product_cat' => get_theme_mod('pastry_bakers_home_product'),
            'order' => 'ASC'
          );
          $loop = new WP_Query( $pastry_bakers_args );
          while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
            <div class="products-box">
              <div class="product-image">
                <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.esc_url(woocommerce_placeholder_img_src()).'" />'; ?>
              </div>
              <div class="product-content text-center pt-3">
                <span class="woo-cat"><?php echo wc_get_product_category_list( $product->get_id(),); ?></span>
                <h3><a href="<?php echo esc_url(get_permalink( $loop->post->ID )); ?>"><?php the_title(); ?></a></h3>
                <p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?> m-0"><?php echo $product->get_price_html(); ?></p>
              </div>
            </div>
          <?php endwhile; wp_reset_query(); ?>
        <?php } ?>
      </div>
    </div>
  </section>
  <section id="page-content">
    <div class="container">
      <div class="py-5">
        <?php
          if ( have_posts() ) :
            while ( have_posts() ) : the_post();
              the_content();
            endwhile;
          endif;
        ?>
      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>