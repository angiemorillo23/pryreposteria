=== Pastry Bakers ===
Contributors: TheMagnifico52
Requires at least: 5.0
Tested up to: 6.4
Requires PHP: 7.2
Stable tag: 0.2.7
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Tags: wide-blocks, block-styles, custom-logo, one-column, two-columns, grid-layout, sticky-post, custom-background, custom-colors, custom-header, custom-menu, featured-images, flexible-header, threaded-comments, theme-options, left-sidebar, right-sidebar, full-width-template, editor-style, e-commerce, blog, food-and-drink, post-formats

Pastry Bakers is a delightful and versatile website template designed specifically for pastry chefs, bakeries, dessert shops, and confectionery businesses. This theme offers a sweet and visually appealing design that captures the essence of delectable treats while providing a robust platform for showcasing culinary creations and attracting customers.

== Description ==

Pastry Bakers is a delightful and versatile website template designed specifically for pastry chefs, bakeries, dessert shops, and confectionery businesses. This theme offers a sweet and visually appealing design that captures the essence of delectable treats while providing a robust platform for showcasing culinary creations and attracting customers. At its core, the Pastry Bakers theme is tailored to meet the unique needs of those in the pastry and baking industry. It serves as a digital storefront, allowing pastry chefs and bakers to display their delicious cakes, pastries, cupcakes, and other delectable treats with elegance and style. Whether you're a small bakery or a renowned pastry chef, this theme empowers you to create an enticing online presence. The theme's customization options are a highlight, enabling users to personalize their websites to align with their brand identity. You can easily choose from various color schemes, fonts, and layout settings to create a website that reflects your unique style and culinary aesthetics. The user-friendly interface makes it accessible to both tech-savvy and non-technical users, ensuring a hassle-free website-building experience. The design of the Pastry Bakers theme is visually captivating, with high-quality images, sliders, and galleries that beautifully showcase mouthwatering treats. The responsive layout ensures that the website looks and functions flawlessly on all devices, from desktop computers to smartphones, enhancing the user experience for visitors and potential customers.

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Pastry Bakers in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.

== Copyright ==

Pastry Bakers WordPress Theme, Copyright 2023 TheMagnifico52
Pastry Bakers is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Pastry Bakers includes support for Infinite Scroll in Jetpack.

== Credits ==

Pastry Bakers bundles the following third-party resources:

Bootstrap, Copyright (c) 2011-2019 Twitter, Inc.
**License:** MIT
Source: https://github.com/twbs/bootstrap

Font Awesome Free 5.6.3 by @fontawesome - https://fontawesome.com
 * License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)

OwlCarousel
 * Copyright 2013-2017 David Deutsch
 * https://github.com/OwlCarousel2/OwlCarousel2
 * https://github.com/OwlCarousel2/OwlCarousel2/blob/develop/LICENSE

Bootstrap
 * Bootstrap v4.3.1 (https://getbootstrap.com/)
 * Copyright 2011-2019 The Bootstrap Authors
 * Copyright 2011-2019 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

PSR-4 autoloader
  * Justin Tadlock
  * License: https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
  * Source: https://github.com/WPTRT/autoload

Webfonts Loader
  * https://github.com/WPTT/webfont-loader
  * License: https://github.com/WPTT/webfont-loader/blob/master/LICENSE

CustomizeSectionButton
  * Justin Tadlock
  * Copyright 2019, Justin Tadlock.
  * License: https://www.gnu.org/licenses/gpl-2.0.html GPL-2.0-or-later
  * https://github.com/WPTRT/customize-section-button

TGMPA
  * GaryJones Copyright (C) 1989, 1991
  * https://github.com/TGMPA/TGM-Plugin-Activation/blob/develop/LICENSE.md
  * License: GNU General Public License v2.0

Pxhere Images,
	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://pxhere.com/en/license

	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://pxhere.com/en/photo/1330777

  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pxhere.com/en/photo/893389

  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pxhere.com/en/photo/1223363

  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pxhere.com/en/photo/1330895

  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pxhere.com/en/photo/660112


== Changelog ==

= 0.1

* Initial release.

= 0.2

* Reviewer bugs resolved.

= 0.2.1

* Added urls.
* Added pro urls in customizer.
* Added get start.
* Added demo css.
* Added general setting.
* Added preloader on of setting.
* Added footer credit link.
* Added site title on of option in customizer.
* Added site tagline on of option in customizer.
* Added logo resize setting.
* Added footer columns.
* Added preloader color settings.

= 0.2.2

* Added post formats.
* Added tag post formats.
* Added woocommerce product per row settings.
* Added scroll to top on off settings.
* Added scroll to top positions option in customizer.
* Added footer copyright on of option in customizer.

= 0.2.3

* Added post setting in customizer.
* Added post page thumbnail on off setting in customizer.
* Added post page title on off setting in customizer.
* Added post page meta on off setting in customizer.

= 0.2.4

* Added slider height option in customizer.
* Added footer background image option in customizer.

= 0.2.5

* Added show/ hide post navigation option in customizer.
* Added copyright background color option in customizer.

= 0.2.6

* Added woocommerce product border radius option in customizer.
* Added site title color option in customizer.
* Added site tagline color option in customizer.

= 0.2.7

* Added upsale button in different section in customizer.
* Added free doc link in get started.
* Done shop page product css in responsive media.
* Done searchform and search page css.