<?php
/**
 * Displays top header
 *
 * @package Pastry Bakers
 */
?>

<div class="top-info text-right py-2 px-5">
	<div class="container">
        <div class="row">
        	<div class="col-lg-7 col-md-7 col-sm-12 col-12 align-self-center">
        		<?php if ( get_theme_mod('pastry_bakers_topbar_text') != "" ) {?>
            		<p class="topbar-text m-0 text-left"><?php echo esc_html(get_theme_mod('pastry_bakers_topbar_text')); ?></p> 
       			 <?php }?>
        	</div>
        	<div class="col-lg-5 col-md-5 col-sm-12 col-12 align-self-center">
        		<div class="row">
        			<div class="col-lg-6 col-md-6 col-sm-6 col-12 align-self-center text-right phone-box">
        				<?php if ( get_theme_mod('pastry_bakers_topbar_phone_text') != "" || get_theme_mod('pastry_bakers_topbar_phone_number') != "" ) {?>
		            		<p class="phone-text m-0"><span><?php echo esc_html(get_theme_mod('pastry_bakers_topbar_phone_text')); ?></span><a href="tel:<?php echo esc_url(get_theme_mod('pastry_bakers_topbar_phone_number')); ?>"><?php echo esc_html(get_theme_mod('pastry_bakers_topbar_phone_number')); ?></a></p> 
		       			<?php }?>
        			</div>
        			<div class="col-lg-6 col-md-6 col-sm-6 col-12 align-self-center right-box">
        				<?php if ( get_theme_mod('pastry_bakers_support_button_url') != "" || get_theme_mod('pastry_bakers_support_button_text') != "" ) {?>
		            		<span class="top-btn mr-3"><a href="<?php echo esc_url(get_theme_mod('pastry_bakers_support_button_url')); ?>"><?php echo esc_html(get_theme_mod('pastry_bakers_support_button_text')); ?></a></span> 
		       			<?php }?>
		       			<?php if ( get_theme_mod('pastry_bakers_faq_button_url') != "" || get_theme_mod('pastry_bakers_faq_button_text') != "" ) {?>
		            		<span class="top-btn mr-3"><a href="<?php echo esc_url(get_theme_mod('pastry_bakers_faq_button_url')); ?>"><?php echo esc_html(get_theme_mod('pastry_bakers_faq_button_text')); ?></a></span> 
		       			<?php }?>
                        <?php if(class_exists('GTranslate')){ ?>
                            <span class="text-center translate-btn">
                                <?php echo do_shortcode('[gtranslate]', 'pastry-bakers');?>
                            </span>
                        <?php }?>
        			</div>
        		</div>
        	</div>
        </div>
	</div>
</div>